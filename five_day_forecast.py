import time
import requests
from location_query import extract_location_data
#from insert_to_table import add_data, get_data
from collections import namedtuple

#QUERY = 'sydney australia'

LOCTYPE = 'locid'

url = 'https://api.weather.com/v3/wx/forecast/daily/5day?placeid={}&units=m&language=en-US&format=json&apiKey={}'

#chosenLoc = extract_location_data(QUERY, LOCTYPE)

#PLACE_ID = chosenLoc[0][1]

##################################################
### MAIN FUNCTION STARTS HERE ##
##################################################

DailySummary = {}
features = ["dayOfWeek", "expirationTimeUtc", "moonPhase", "moonPhaseCode", "moonPhaseDay","moonriseTimeLocal",\
            "moonriseTimeUtc", "moonsetTimeLocal", "moonsetTimeUtc", "narrative", "qpf","qpfSnow",\
            "sunriseTimeLocal", "sunriseTimeUtc", "sunsetTimeLocal", "sunsetTimeUtc", "temperatureMax","temperatureMin",\
            "validTimeLocal", "validTimeUtc"]
Forecast = namedtuple("Forecast", features)

def extract_weather_data(place_id):
    api_key = '06e69919b82142bfa69919b821f2bf8e'
    request = url.format(place_id, api_key)        
    print(request)
    response = requests.get(request)
    if response.status_code == 200:
        for i in range(len(response.json()['dayOfWeek'])):            
            DailySummary[i]=Forecast(
                #date=target_date,
                dayOfWeek = response.json()['dayOfWeek'][i], 
                expirationTimeUtc = response.json()['expirationTimeUtc'][i],
                moonPhase = response.json()['moonPhase'][i],
                moonPhaseCode = response.json()['moonPhaseCode'][i],
                moonPhaseDay = response.json()['moonPhaseDay'][i],
                moonriseTimeLocal = response.json()['moonriseTimeLocal'][i],
                moonriseTimeUtc = response.json()['moonriseTimeUtc'][i],
                moonsetTimeLocal = response.json()['moonsetTimeLocal'][i],
                moonsetTimeUtc = response.json()['moonsetTimeUtc'][i],
                narrative = response.json()['narrative'][i],
                qpf = response.json()['qpf'][i],
                qpfSnow = response.json()['qpfSnow'][i],
                sunriseTimeLocal = response.json()['sunriseTimeLocal'][i],
                sunriseTimeUtc = response.json()['sunriseTimeUtc'][i],
                sunsetTimeLocal = response.json()['sunsetTimeLocal'][i],
                sunsetTimeUtc = response.json()['sunsetTimeUtc'][i],
                temperatureMax = response.json()['temperatureMax'][i],
                temperatureMin = response.json()['temperatureMin'][i],
                validTimeLocal = response.json()['validTimeLocal'][i],
                validTimeUtc = response.json()['validTimeUtc'][i])
    time.sleep(6)
        
    return DailySummary

#DailySummary = extract_weather_data(PLACE_ID, API_KEY)

#add_data(DailySummary)

#get_data()