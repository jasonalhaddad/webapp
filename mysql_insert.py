from flask import Flask
from flaskext.mysql import MySQL

mysql = MySQL()
app = Flask(__name__)
app.secret_key = 'spooky action at a distance-Einstein'

# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'GrainDynamics2019@'
app.config['MYSQL_DATABASE_DB'] = 'FlaskBlogApp'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)


#sql = "INSERT INTO blog_user (user_name, user_username) VALUES (%s, %s)"
#val = ("John", "Highway 21")
#mycursor.execute(sql, val)
 
def insert_to_table(name, email, password):
    success = 0 
    con = mysql.connect()
    mycursor = con.cursor()
    mycursor.callproc('sp_createUser',(name,email,password))
    data = mycursor.fetchall()

    if len(data) is 0:
        con.commit()
        success = 1
        print('User created successfully !')
    else:
        success = 0
        print('error: ' + str(data[0]))
    
    mycursor.close()
    con.close()
    return success

