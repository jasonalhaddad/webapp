import sqlite3
import re
##############################################################################
########################FOR HOME ##########################################
##############################################################################
# function to insert data on a table
def change_home(name, placeID):    
    conn=sqlite3.connect('settings.db')
    curs=conn.cursor()
    curs.execute("DROP TABLE local_settings;")
    curs.execute("CREATE TABLE local_settings (locationname text NOT NULL, locationid text NOT NULL);")
    set_location = "INSERT INTO local_settings VALUES(?,?)"
    curs.execute(set_location,[name,placeID])
    conn.commit()
    conn.close()
    
# call the function to insert data
def get_home():
    
    conn=sqlite3.connect('settings.db')
    curs=conn.cursor()
    # print database content
    print ("Current Home Location:\n")
    curs.execute("SELECT locationname FROM local_settings;")
    nameresult = curs.fetchall()
    print(nameresult)
    curs.execute("SELECT locationid FROM local_settings;")
    idresult = curs.fetchall()
    print (idresult)
    result = nameresult + idresult
    print(result)
    return result
    conn.close()
    
# close the database after use
##############################################################################
########################FOR GENERAL SETTINGS##########################################
##############################################################################
# function to insert data on a table
def change_settings(settings_detla):    
    conn=sqlite3.connect('settings.db')
    curs=conn.cursor()
    #curs.execute("DROP TABLE local_settings;")
    #curs.execute("CREATE TABLE local_settings (locationname text NOT NULL, locationid text NOT NULL);")
    set_location = "INSERT INTO local_settings VALUES(?,?)"
    curs.execute(set_location,[name,placeID])
    conn.commit()
    conn.close()
    
# call the function to insert data
def get_settings():
    conn=sqlite3.connect('settings.db')
    curs=conn.cursor()
    # print database content
    print ("Current Home Location:\n")
    curs.execute("SELECT locationname FROM local_settings;")
    nameresult = curs.fetchall()
    print(nameresult)
    curs.execute("SELECT locationid FROM local_settings;")
    idresult = curs.fetchall()
    print (idresult)
    result = nameresult + idresult
    print(result)
    return result
    conn.close()

##############################################################################
########################FOR WEATHER##########################################
##############################################################################
# function to insert data on a table
def add_weather_data (DailySummary):
    conn=sqlite3.connect('weatherData.db')
    curs=conn.cursor()
    for i in range(len(DailySummary)):
        curs.execute("INSERT INTO LOCALWEATHER_data values(datetime('now'), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?))", (DailySummary[i][0],DailySummary[i][1], DailySummary[i][2], DailySummary[i][3], DailySummary[i][4],DailySummary[i][5],DailySummary[i][6],DailySummary[i][7],DailySummary[i][8],DailySummary[i][9],DailySummary[i][10],DailySummary[i][11],DailySummary[i][12],DailySummary[i][13],DailySummary[i][14],DailySummary[i][15],DailySummary[i][16],DailySummary[i][17],DailySummary[i][18],DailySummary[i][19]))
        conn.commit()
    conn.close()
    
# call the function to insert data
def get_weather_data():
    conn=sqlite3.connect('weatherData.db')
    curs=conn.cursor()
    # print database content
    print ("\nEntire database contents:\n")
    for row in curs.execute("SELECT * FROM LOCALWEATHER_data ORDER BY queryTimestamp DESC LIMIT 1"):
        time = str(row[0])
        tempMax = row[17]
        tempMin = row[18]
    conn.close()
    return time, tempMax, tempMin
# close the database after use

###############################################################################
