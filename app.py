#################################################################################
#!/usr/bin/env python
from importlib import import_module
import os
import time
import serial
import re
from flask import Flask, render_template, Response, request, json, redirect, session
from insert_to_table import get_home, change_home
from location_query import extract_location_data
from flaskext.mysql import MySQL
from werkzeug import generate_password_hash, check_password_hash
from mysql_insert import insert_to_table
from five_day_forecast import extract_weather_data

import RPi.GPIO as GPIO#

# import camera driver
if os.environ.get('CAMERA'):
    Camera = import_module('camera_' + os.environ['CAMERA']).Camera
else:
#    from camera import Camera

# Raspberry Pi camera module (requires picamera package)
    from camera_pi import Camera

app = Flask(__name__, static_url_path='/static')

#################################################################################
#########################SERIAL STUFF#############################################
#################################################################################

ser = serial.Serial('/dev/ttyACM0',9600)

def serial_interact(tosend):
    ser.flush()
    time.sleep(0.1)
    ser.write(str.encode(tosend))
    time.sleep(0.1)
    #read_serial = parse_serial(str(ser.readline(), 'utf-8'))
    read_serial = parse_serial(str(ser.readline(), 'utf-8'))
    print(read_serial)
    return read_serial

def parse_serial(msg_in):
    try:
        found = re.search('<(.*)>', msg_in).group(1)
    except AttributeError:
        found = '' # apply your error handling
    return found

   
@app.route('/')
def index():
   # os.system("echo '1-1' |sudo tee /sys/bus/usb/drivers/usb/bind") # turn on USB
   msg = "[Grain_Temperature]"
   Grain_Temp = serial_interact(str(msg))
   time.sleep(1)
   
   msg = "[Base_Temperature]"
   Base_Temp = serial_interact(str(msg))
   time.sleep(1)
   
   msg = "[Grain_Humidity]"
   Grain_Humidity = serial_interact(str(msg))
   time.sleep(1)
   
   msg = "[Base_Humidity]"
   Base_Humidity = serial_interact(str(msg))
   time.sleep(0.1)
   
   msg = "[Toggle_Red]"
   Red_State = serial_interact(str(msg))
   time.sleep(0.5)
   
   msg = "[Toggle_Green]"
   Green_State = serial_interact(str(msg))
   time.sleep(0.5)
      
   msg = "[Toggle_Red]"
   Red_State = serial_interact(str(msg))
   time.sleep(0.5)
   
   msg = "[Toggle_Green]"
   Green_State = serial_interact(str(msg))
   time.sleep(0.5)
   
   results = {"Grain_Temp" : Grain_Temp,
           "Base_Temp" : Base_Temp,
           "Grain_Humidity" : Grain_Humidity,
           "Base_Humidity" : Base_Humidity,
           "Red_State" : Red_State,
           "Green_State" : Green_State}
   return render_template('index.html', **results) 
   # os.system("echo '1-1' |sudo tee /sys/bus/usb/drivers/usb/unbind") # turn off USB
   
#################################################################################
#########################VIDEO STUFF#############################################
#################################################################################

def gen(camera):
    """Video streaming generator function."""
    while True:
        frame = camera.get_frame()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')

@app.route('/video_feed')
def video_feed():
    """Video streaming route. Put this in the src attribute of an img tag."""
    return Response(gen(Camera()),
                    mimetype='multipart/x-mixed-replace; boundary=frame')

#################################################################################
#########################LOCATION STUFF#############################################
#################################################################################
@app.route('/location')
def location():
    ## needs to pass name not ID
    home = get_home()
    
    LOCTYPE = 'locid'
    

    print(home[0][0])
    print("^home") 
    results = extract_location_data(home[0][0], LOCTYPE)
    time.sleep(1)
    
    DailySummary = extract_weather_data(home[1][0])
    print(home[1][0])
    change_home(home[0][0],home[1][0])

    time.sleep(1)

    options = {"placeID" : home[1][0],
               "name":  home[0][0],
               "day1" :DailySummary[0][0],
               "day1_narrative" :DailySummary[0][9],
               "option1" : results[0][0],
               "option1_placeid" : results[0][1],
               "option2" : results[1][0],
               "option2_placeid" : results[1][1],
               "option3" : results[2][0],
               "option3_placeid" : results[2][1],
               "option4" : results[3][0],
               "option4_placeid" : results[3][1],
               "option5" : results[4][0],
               "option5_placeid" : results[4][1],
               "option6" : results[5][0],
               "option6_placeid" : results[5][1]} 
    return render_template('location.html', **options)

@app.route('/location', methods=['POST'])
#Form for location input
def my_form_post():
    LOCTYPE = 'locid'#
    text = request.form['text']
    processed_text = text.upper()
    results = extract_location_data(processed_text, LOCTYPE)
    
    options = {"option1" : results[0][0],
               "option1_placeid" : results[0][1],
               "option2" : results[1][0],
               "option2_placeid" : results[1][1],
               "option3" : results[2][0],
               "option3_placeid" : results[2][1],
               "option4" : results[3][0],
               "option4_placeid" : results[3][1],
               "option5" : results[4][0],
               "option5_placeid" : results[4][1],
               "option6" : results[5][0],
               "option6_placeid" : results[5][1]} 
#api call to get the real location here    
    return render_template('location.html', **options)

@app.route('/set-location/<placeID>/<name>')
def set_location(name, placeID):
    DailySummary = extract_weather_data(placeID)
    print(str(name))
    change_home(name,placeID)
    narratives = {"name": name,
                  "day1" :DailySummary[0][0],
                  "day1_narrative" :DailySummary[0][9],}
    return render_template('location.html', **narratives)


#################################################################################
#########################SETTINGS STUFF#############################################
#################################################################################
@app.route('/settings')
def settings_init():
    settings = get_settings()
    fetched_data = {"something": settings
                   }
    return render_template('settings.html', **fetched_data)
#################################################################################
#########################SIGNUP STUFF#############################################
#################################################################################

mysql = MySQL()
app.secret_key = 'spooky action at a distance-Einstein'

# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'GrainDynamics2019@'
app.config['MYSQL_DATABASE_DB'] = 'FlaskBlogApp'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)


@app.route('/showSignUp')
def showSignUp():
    return render_template('signup.html')

@app.route('/showSignin')
def showSignin():
    return render_template('signin.html')

@app.route('/userHome')
def userHome():
    if session.get('user'):
        return render_template('userHome.html')
    else:
        return render_template('error.html',error = 'Unauthorized Access')

@app.route('/logout')
def logout():
    session.pop('user', None)
    return redirect('/')

@app.route('/validateLogin',methods=['POST'])
def validateLogin():
    con = mysql.connect()
    cursor = con.cursor()
    try:
        _username = request.form['inputEmail']
        _password = request.form['inputPassword']
               
        # connect to mysql

        cursor.callproc('sp_validateLogin',(_username,))
        data = cursor.fetchall()

        if len(data) > 0:
            if check_password_hash(str(data[0][3]),_password):
                session['user'] = data[0][0]
                return redirect('/userHome')
            else:
                return render_template('error.html',error = 'Wrong Email address or Password.')
        else:
            return render_template('error.html',error = 'Wrong Email address or Password.')          

    except Exception as e:
        return render_template('error.html',error = str(e))
    finally:
        cursor.close()
        con.close()

@app.route('/signUp',methods=['POST','GET'])
def signUp():

    _name = request.form['inputName']
    _email = request.form['inputEmail']
    _password = request.form['inputPassword']
    
    if _name and _email and _password:            
        _hashed_password = generate_password_hash(_password)
        print(_hashed_password)
        success = insert_to_table(_name,_email,_hashed_password)
        return success
    else:
        return json.dumps({'html':'<span>Enter the required fields</span>'})

#################################################################################
##############################END################################################
#################################################################################


if __name__ == '__main__':
    app.run(host='0.0.0.0', threaded=True)

