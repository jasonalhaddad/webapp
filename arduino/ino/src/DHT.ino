#include <dht.h>

dht DHT_BASE;
dht DHT_GRAIN;

#define DHT_BASE_PIN 7
#define DHT_GRAIN_PIN 6

#define LED_GREEN 8
#define LED_RED 9

char incomingString;
char command;



// Example 5 - Receive with start- and end-markers combined with parsing

const byte numChars = 32;
char receivedChars[numChars];
char tempChars[numChars];        // temporary array for use when parsing

// variables to hold the parsed data
char messageFromPC[numChars] = {0};

boolean newData = false;

//============

void setup() {
    Serial.begin(9600);
    pinMode(LED_GREEN, OUTPUT);
    pinMode(LED_RED, OUTPUT);
}

//============

void loop() {
    recvWithStartEndMarkers();
    if (newData == true) {
        strcpy(tempChars, receivedChars);
            // this temporary copy is necessary to protect the original data
            //   because strtok() used in parseData() replaces the commas with \0
        parseData();
        //showParsedData();
        decideAndReturn();
        newData = false;
    }
}


//============
void doDHTandLED()
{
      command = incomingString;
      digitalWrite(LED_GREEN,HIGH);      
      int chk = DHT_BASE.read11(DHT_BASE_PIN);
      Serial.print("[Base_Temperature = ");
      Serial.print(DHT_BASE.temperature);
      Serial.println("]");
      Serial.print("Base_Humidity = ");
      Serial.print(DHT_BASE.humidity);
      Serial.println("]");
      delay(1000);
      digitalWrite(LED_GREEN,LOW);
    
      digitalWrite(LED_RED,HIGH);
      int chkgr = DHT_GRAIN.read11(DHT_GRAIN_PIN);
      Serial.print("[Grain_Temperature = ");
      Serial.print(DHT_GRAIN.temperature);
      Serial.println("]");
      
      Serial.print("[Grain_Humidity = ");
      Serial.print(DHT_GRAIN.humidity);
      Serial.println("]");
      delay(1000);
      digitalWrite(LED_RED,LOW);
  
}

//============
void recvWithStartEndMarkers() {
    static boolean recvInProgress = false;
    static byte ndx = 0;
    char startMarker = '[';
    char endMarker = ']';
    char rc;

    while (Serial.available() > 0 && newData == false) {
        rc = Serial.read();

        if (recvInProgress == true) {
            if (rc != endMarker) {
                receivedChars[ndx] = rc;
                ndx++;
                if (ndx >= numChars) {
                    ndx = numChars - 1;
                }
            }
            else {
                receivedChars[ndx] = '\0'; // terminate the string
                recvInProgress = false;
                ndx = 0;
                newData = true;
            }
        }

        else if (rc == startMarker) {
            recvInProgress = true;
        }
    }
}

//============

void parseData() {      // split the data into its parts

    char * strtokIndx; // this is used by strtok() as an index

    strtokIndx = strtok(tempChars,",");      // get the first part - the string
    strcpy(messageFromPC, strtokIndx); // copy it to messageFromPC

}

//============

void showParsedData() {
    Serial.println(messageFromPC);
}

void decideAndReturn() {
    if(strcmp(messageFromPC,"Grain_Temperature")==0) {
      int chkgr = DHT_GRAIN.read11(DHT_GRAIN_PIN);
      Serial.print("<");
      Serial.print(DHT_GRAIN.temperature);
      Serial.println(">");
    }
    if(strcmp(messageFromPC,"Grain_Humidity")==0) {
      int chkgr = DHT_GRAIN.read11(DHT_GRAIN_PIN);
      Serial.print("<");
      Serial.print(DHT_GRAIN.humidity);
      Serial.println(">");
    }
    if(strcmp(messageFromPC,"Base_Temperature")==0) {
      int chkgr = DHT_BASE.read11(DHT_BASE_PIN);
      Serial.print("<");
      Serial.print(DHT_BASE.temperature);
      Serial.println(">");
    }
    if(strcmp(messageFromPC,"Base_Humidity")==0) {
      int chkba = DHT_BASE.read11(DHT_BASE_PIN);
      Serial.print("<");
      Serial.print(DHT_BASE.humidity);
      Serial.println(">");
    }
   if(strcmp(messageFromPC,"Toggle_Red")==0) {
      if (digitalRead(LED_RED) == HIGH) {
          digitalWrite(LED_RED,LOW);
      } 
      else
      {
            digitalWrite(LED_RED,HIGH);
      }
      Serial.print("<");
      Serial.print(digitalRead(LED_RED));
      Serial.println(">"); 
    }
    if(strcmp(messageFromPC,"Toggle_Green")==0) {

      if (digitalRead(LED_GREEN) == HIGH) {
          digitalWrite(LED_GREEN,LOW);
      } 
      else
      {
            digitalWrite(LED_GREEN,HIGH);
      }
      Serial.print("<");
      Serial.print(digitalRead(LED_GREEN));
      Serial.println(">"); 
    }
}
