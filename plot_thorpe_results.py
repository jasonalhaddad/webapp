#from pandas import read_csv
#from pandas import DataFrame
#from pandas import Grouper
from matplotlib import pyplot
import pandas as pd

############################################################################################
##################https://machinelearningmastery.com/time-series-data-visualization-with-python/
############################################################################################


#series = read_csv('daily-min-temperatures.csv', header=0, index_col=0, parse_dates=True, squeeze=True)
#groups = series.groupby(Grouper(freq='A'))
#years = DataFrame()
#for name, group in groups:
#	years[name.year] = group.values
#years = years.T

data = pd.read_excel (r'MATLAB/Plots.xls') #for an earlier version of Excel, you may need to use the file extension of 'xls'
df = pd.DataFrame(data)
df = df.drop('Var1',axis=1)
df = df.drop(df.index[0])
print (df)

pyplot.matshow(df, interpolation=None, aspect='auto')
pyplot.show()
#pyplot.savefig('surfaceplotexample.png', transparent=True)
#pyplot.show()
