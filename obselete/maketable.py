import sqlite3 as lite
import sys
con = lite.connect('weatherData.db')
with con: 
    cur = con.cursor() 
    cur.execute("DROP TABLE IF EXISTS LOCALWEATHER_data")
    cur.execute("CREATE TABLE LOCALWEATHER_data(queryTimestamp DATETIME, dayOfWeek TEXT, expirationTimeUtc DATETIME, \
                                                moonPhase TEXT, moonPhaseCode TEXT, moonPhaseDay, TEXT, \
                                                moonriseTimeLocal TEXT, moonriseTimeUtc DATETIME, moonsetTimeLocal DATETIME, \
                                                moonsetTimeUtc DATETIME, narrative TEXT, qpf NUMERIC, qpfSnow NUMERIC \
                                                sunriseTimeLocal DATETIME, sunriseTimeUtc DATETIME, sunsetTimeLocal DATETIME, \
                                                sunsetTimeUtc DATETIME, temperatureMax NUMERIC, temperatureMin NUMERIC,\
                                                validTimeLocal DATETIME, validTimeUtc DATETIME)")