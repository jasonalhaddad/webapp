import matplotlib
import matplotlib.pyplot as plt
import numpy as np
#from test import df

def plot_relationships(predictors,df2):

    # manually set the parameters of the figure to and appropriate size
    plt.rcParams['figure.figsize'] = [16, 22]
    
    # call subplots specifying the grid structure we desire and that 
    # the y axes should be shared
    fig, axes = plt.subplots(nrows=6, ncols=3, sharey=True)
    
    # Since it would be nice to loop through the features in to build this plot
    # let us rearrange our data into a 2D array of 6 rows and 3 columns
    arr = np.array(predictors).reshape(6, 3)
    
    # use enumerate to loop over the arr 2D array of rows and columns
    # and create scatter plots of each meantempm vs each feature
    for row, col_arr in enumerate(arr):
        for col, feature in enumerate(col_arr):
            axes[row, col].scatter(df2[feature], df2['tempAvg'])
            if col == 0:
                axes[row, col].set(xlabel=feature, ylabel='tempAvg')
            else:
                axes[row, col].set(xlabel=feature)
    plt.show()
    
##################################################
###START OUTLIER CHECK###
##################################################
def plot_outliers(var_to_check, var_to_check_name, df):
    #def outlier_check():
    # Call describe on df and transpose it due to the large number of columns
    spread = df.describe().T
    
    # precalculate interquartile range for ease of use in next calculation
    IQR = spread['75%'] - spread['25%']
    
    # create an outliers column which is either 3 IQRs below the first quartile or
    # 3 IQRs above the third quartile
    spread['outliers'] = (spread['min']<(spread['25%']-(3*IQR)))|(spread['max'] > (spread['75%']+3*IQR))
    
    # just display the features containing extreme outliers
    spread.loc[spread.outliers,]
    
    plt.rcParams['figure.figsize'] = [14, 8]
    var_to_check.hist()
    title = 'Distribution of values of {}'
    title = title.format(var_to_check_name)
    plt.title(title)
    plt.xlabel(var_to_check_name)
    plt.show()


##################################################
###END OUTLIER CHECK###
##################################################