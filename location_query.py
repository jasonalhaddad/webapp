import time
import requests
from collections import namedtuple


#https://api.weather.com/v2/pws/history/hourly?stationId=ISYDNEY529&format=json&units=m&date=20181001&apiKey=06e69919b82142bfa69919b821f2bf8e
#BASE_URL = "http://api.wunderground.com/api/{}/history_{}/q/AU/Sydney.json"


#https://api.weather.com/v3/location/search?query=atlanta&locationType=locid&language=en-US&format=json&apiKey=yourApiKey 

url = "https://api.weather.com/v3/location/search?query={}&locationType={}&language=en-US&format=json&apiKey={}"
features = ["displayName", "placeID"]

Locations = namedtuple("Locations", features)

def extract_location_data(query, loctype):
    api_key = '06e69919b82142bfa69919b821f2bf8e'
    possibleLocs = {}
    request = url.format(query, loctype, api_key)
    print(request)
    response = requests.get(request)
    if response.status_code == 200:
        for i in range(len(response.json()['location']['displayName'])):
            data = response.json()['location']            
            possibleLocs[i] = Locations(displayName=data['displayName'][i], 
                        placeID=data['placeId'][i])
    time.sleep(1)
    
    return possibleLocs