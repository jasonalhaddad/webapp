data = xlsread('Plots.xls');
time_in_days = data(1,2:end);
depth = data(2:end,1);
temperature = data(2:end,2:end);
contourf(time_in_days, depth, temperature)
