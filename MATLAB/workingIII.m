%*********************************************
%TO SIMULATE THE PERFORMANCE OF A BED OF AERATED GRAINS
%*********************************************

function results = main()
self = {};
self = setInitVals;

L = log4matlab('log of SELF.log');
ScriptName = 'workingiii';

CYCLES = 25;
HOURLY_INTERVAL = 50;

for KKK = 1:CYCLES
    for KK = 1:HOURLY_INTERVAL
        %UPDATE TIME
        self.TIME = self.TIME + self.DT;
        for I = 2:self.NXM1
            self.I = I;
            self = SatVapPres(self);
            %SOLVE MOISTURE CONSERVATION EQUATION ALONG THE BED
            self.DHDX = (self.H(I) - self.H(I - 1)) / self.DX;
            self.DWDT = -self.FA / self.RHOB * self.DHDX + self.RHOA * self.DEFF / (self.RHOB * self.DX ^ 2) * (self.H(I - 1) - 2 * self.H(I) + self.H(I + 1)); 

%             if isnan(self.DWDT) || isinf(self.DWDT) || self.DWDT == 0 || isnan(self.DHDX) || isinf(self.DHDX) || self.DHDX == 0
%                 DHDX = self.DHDX
%                 DWDT = self.DWDT
%                 I
%                 H = self.H(I)
%                 Hm1= self.H(I-1)
%                 Hp1 = self.H(I+1)
%                 keyboard
%             end
            
%             if isnan(self.DWDT) || isinf(self.DWDT) || self.DWDT == 0 || isnan(self.DHDX) || isinf(self.DHDX) || self.DHDX == 0
%                 L.mlog = {L.DEBUG,ScriptName,'=========================================================================================='};
%                 L.mlog = {L.DEBUG,ScriptName,'CYCLE: '};
%                 L.mlog = {L.DEBUG,ScriptName,L.MatrixToString(KKK)};  
%                 L.mlog = {L.DEBUG,ScriptName,'HOUR: '};
%                 L.mlog = {L.DEBUG,ScriptName,L.MatrixToString(KK)};  
%                 L.mlog = {L.DEBUG,ScriptName,'ITERATION: '};
%                 L.mlog = {L.DEBUG,ScriptName,L.MatrixToString(I)};   
%                 L.mlog = {L.DEBUG,ScriptName,'DHDX IS NOW: '};
%                 L.mlog = {L.DEBUG,ScriptName,L.MatrixToString(self.DHDX)};   
%                 L.mlog = {L.DEBUG,ScriptName,'DWDT IS NOW: '};
%                 L.mlog = {L.DEBUG,ScriptName,L.MatrixToString(self.DWDT)};   
%                 L.mlog = {L.DEBUG,ScriptName,'H(I) IS NOW: '};
%                 L.mlog = {L.DEBUG,ScriptName,L.MatrixToString(self.H(I))};  
%                 L.mlog = {L.DEBUG,ScriptName,'H(I-1) IS NOW: '};
%                 L.mlog = {L.DEBUG,ScriptName,L.MatrixToString(self.H(I-1))};  
%                 L.mlog = {L.DEBUG,ScriptName,'=========================================================================================='};
%             end
            %THIS IS EQUATION 4.132 WITH A TERM THAT ACCOUNTS FOR THE
            %DIFFUSION OF MOISTURE.
            self.WNEW(I) = self.W(I) + self.DWDT * self.DT;    
            %% SOLVE THERMAL ENERGY EQUATION
            self = SatVapPres(self);
            self.DENOM = (self.CG + self.CW * self.W(I)) * self.RHOB + self.EPS * self.RHOA * (self.CA + self.H(I) * (self.CW + self.DHVDT));
            self.A = self.RHOB *self.hs * self.DWDT;
            self.DTDX = (self.T(I) - self.T(I - 1)) / self.DX;
            self.B = -self.FA * (self.CA + self.H(I) * (self.CW + self.DHVDT)) * self.DTDX;
            self.C = self.KEFF * (self.T(I - 1) - 2 * self.T(I) + self.T(I + 1)) / self.DX ^ 2;
            %EQUATION 4.131
            self.TNEW(I) = self.T(I) + self.DT * (self.A + self.B + self.C) / self.DENOM;
            %disp("HS = " + self.hs + ", HVAP = " + self.HVAP + ", HW = " + self.HW + ", RH = " + self.RH + ", H(I) = " + self.H(I));
            %% CALCULATE BOUNDARY CONDITIONS AT EXIT OF THE BED
            self.TNEW(self.NX) = self.TNEW(self.NX - 1);
            self.H(self.NX) = self.H(self.NX - 1);
            self.WNEW(self.NX) = self.WNEW(self.NX - 1);
        end
        for I = 2:self.NX
            self.T(I) = self.TNEW(I);
            self.W(I) = self.WNEW(I);
        end

    end

    for I = 1:self.NX
        self.M = self.W(I) / (1 + self.W(I));
        %disp("I = " + I + ", T(I) = " + self.T(I) + ", H(I) = " +  self.H(I) + ", M = " + self.M);
        localResults{I} = {I;self.T(I);self.H(I);self.M;};
    end
    results{KKK} = localResults;
end

%% FOR PLOTTING
for K = 1:KKK %% For all reps
    for I = 1:self.NX %%For all nodes
        tempDataToPlot(K,I) = cell2mat(results{K}{I}(2));
        humDataToPlot(K,I) = cell2mat(results{K}{I}(3));
    end
end

rows=1:self.NX';
T_table = table(rows',tempDataToPlot(:,:)');

T_INTERVAL = HOURLY_INTERVAL:HOURLY_INTERVAL:self.TIME/3600;

T_table{1,2:end} = T_INTERVAL;

filename = 'Plots.xls';

writetable(T_table,filename,'Sheet',1,'Range','A1')

figure;
subplot(2,2,1);
bar(tempDataToPlot(1,:)); %% after first time step
subplot(2,2,2);
bar(tempDataToPlot(2,:)); %% after first time step
subplot(2,2,3);
bar(tempDataToPlot(3,:)); %% after first time step
subplot(2,2,4);
bar(tempDataToPlot(4,:)); %% after first time step

figure;
subplot(2,2,1);
bar(humDataToPlot(1,:)); %% after first time step
subplot(2,2,2);
bar(humDataToPlot(2,:)); %% after first time step
subplot(2,2,3);
bar(humDataToPlot(3,:)); %% after first time step
subplot(2,2,4);
bar(humDataToPlot(4,:)); %% after first time step
end

%%  FUNCTIONS
function self = SatVapPres(self)
%TO CALCULATE SATURATION VAPOUR PRESSURE OF WATER
    self.PS = (6E+25 / ((self.T(self.I) + 273) ^ 5)) * exp(-6800 / (self.T(self.I) + 273));
    %CALCULATE RELATIVE HUMIDITY OF INTERSTITIAL AIR
    self.DPSDT = self.PS / (self.T(self.I) + 273.15) * (-5 + 6800 / (self.T(self.I) + 273.15)); 
    self.RH = exp(-self.ACP / (self.T(self.I) + self.CCP) * exp(-self.BCP * self.W(self.I)));
    self.DRDT = self.ACP * self.RH / (self.T(self.I) + self.CCP) ^ 2 * exp(-self.BCP * self.W(self.I));
    self.hsbyhv = 1 + (self.PS / self.RH) * self.DRDT / self.DPSDT;
    self.HVAP = self.HV + self.DHVDT * self.T(self.I);
    %CALCULATE DIFFERENTIAL HEAT OF SORPTION
    self.hs = self.HV * self.hsbyhv;
    self.P = self.PS * self.RH;
    %CALCULATE INTERSTITIAL HUMIDITY OF AIR.
    self.H(self.I) = (self.P * self.N) / (self.PATM - self.P);
end



function self = DryLoss(self)
%CALCULATE RATE AT WHICH DRY MATTER, DDMDT, IS LOST. THIS USES EQUATIONS 3.138 TO 3.145
self.MCWB = self.W(self.I) * 100 / (1 + self.W(self.I));
self.MCDB = self.W(self.I) * 100;
self.AR = 32.3 * exp(-.1044 * self.T(self.I) - 1.856);
if (self.T(self.I) <= 15 || self.MCWB <= 19);self.TMOD = self.AR;end
if (self.T(self.I) > 15 && self.MCWB > 19 && self.MCWB <= 28);self.TMOD = self.AR + ((self.MCWB - 19) / 100) * exp(.0183 * self.T(self.I) - .28437);end
if (self.T(self.I) > 15 && self.MCWB > 28); self.TMOD = self.AR + 9.000001E-02 * exp(.0183 * self.T(self.I) - .2847);end
self.WMOD = 1;
if (self.MCWB > 13 && self.MCWB <= 35); self.WMOD = .103 * (exp(455 / self.MCDB ^ 1.53) - 8.449999E-03 * self.MCDB + 1.558);end
self.DTEQ = self.DT / (self.TMOD * self.WMOD);
if (self.MCWB < 14); self.DTEQ = 0; end
self.TEQ(self.I) = self.TEQ(self.I) + self.DTEQ;
self.DDMTEQ = 1.472E-09 * exp(1.667E-06 * self.TEQ - 1) + 2.833E-09;
self.DDMDT = self.DDMTEQ / (self.TMOD * self.WMOD);
if self.MCWB < 14; self.DDMDT = 0; end
end

%ASK FOR INPUTS:
function self = setInitVals()
%% definitions
%VEL = FACE VELOCITY OF AIR THROUGH THE GRAIN, M/S.
%T(self.I) = TEMPERATURE OF GRAINS
%H(self.I) = HUMIDITY OF INTERSTITIAL AIR
%% Code Start
%***
%SET COEFFICIENTS IN THE MODIFIED CHUNG-PFOST EQUATION
self.ACP = 921.65;
self.BCP = 18.08;
self.CCP = 112.35;
%***
self.TIME = 0;
self.DT = 3600;%DT = TIME STEP
%REM LENGTH OF BED IS W, AND NUMBER OF NODES IS NX
self.W = 5;%W = LENGTH OF GRAIN BED, M.
self.NX = 21;%NX = NUMBER OF NODES
self.NXM1 = self.NX - 1;
self.DX = self.W /(self.NXM1);%DX = STEP LENGTH
self.EPS = .4;%EPS = VOID FRACTION OF THE BED
self.RHOS = 1300;%RHOS = DENSITY OF GRAIN KERNELS
self.RHOB = (1 - self.EPS) * self.RHOS;
self.RHOA = 1.2;%RHOA = DENSITY OF AIR
self.CG = 1300;%CG = SPECIFIC HEAT OF DRY GRAIN, J/KG/K
self.CW = 4180;%CW = SPECIFIC HEAT OF LIQUID WATER, J/KG/K
self.CA = 1000;%CA = SPECIFIC HEAT OF AIR, J/KG/K
self.KEFF = .15;%KEFF = EFFECTIVE THERMAL CONDUCTIVITY OF GRAIN BED.
self.DEFF = .000005;%DEFF = EFFECTIVE DIFFUSIVITY OF MOISTURE VAPOUR THROUGH GRAIN.
self.FA = .01;%FA = MASS FLOW RATE PER UNIT AREA OF BED, KG/S/M^2
self.HV = 2502390;%HV = LATENT HEAT OF VAPORIZATION OF WATER, J/KG
self.N = .622;%N = RATIO OF MOLECULAR WEIGHTS OF WATER VAPOUR AND AIR.
%LATENT HEAT OF VAPORIZATION OF WATER, HVAP = HV + DHVDT*T
self.DHVDT = -2377;%DHVDT = DIFFERENTIAL OF LATENT HEAT W.R. TEMPERATURE

% prompt = 'What is the humidity of the air to be used for aeration? ';%T(1) = INLET AIR TEMPERATURE
% self.H(1) = input(prompt);%INPUT: HUMIDITY OF AIR USED FOR AERATION
% prompt = 'What is the temperature of the air to be used for aeration? ';%H(1) = HUMIDITY OF INLET AIR
% self.T(1) = input(prompt);%INPUT: TEMPERATURE OF AIR USED FOR AERATION (DEGREES C)

self.H(1) = 20
self.T(1) = 30
self.TIN = 35
self.WIN = 15
H1=self.H(1)
T1=self.T(1)    
TIN=self.TIN
WIN=self.WIN

%AN OPERATIONAL HINT: IF YOU WISH TO INCREASE THE FLOW RATE BY 10, SAY, TO
%SIMULATE A DRYING OPERATION REDUCE THE TIME STEP, DT, BY 10.
% prompt = "INITIAL GRAIN TEMPERATURE (DEGREES C) :";
% self.TIN = input(prompt);
% prompt = "INITIAL GRAIN MOISTURE CONTENT (% WET BASIS) :";
% self.WIN = input(prompt);



%INITIALISE MOISTURE CONTENT AND TEMPERATURE
for I = 2:self.NX
    self.T(I) = self.TIN;
    self.W(I) = self.WIN / (100 - self.WIN);
end

self.H = zeros(1,self.NX);
self.TNEW = zeros(1,self.NX);
self.WNEW = zeros(1,self.NX);
self.TEQ = zeros(1,self.NX);

results = {};

self.PATM = 101325;
self.N = .622;
self.P = self.PATM * (self.H(1) / (self.N + self.H(1)));
self.PS = (6E+25) / ((self.T(1) + 273) ^ 5) * exp(-6800 / (self.T(1) + 273));
self.RH = self.P / self.PS;
self.W(1) = -1 / self.BCP * log(log(self.RH ^ (-(self.T(1) + self.CCP) / self.ACP)));
end

